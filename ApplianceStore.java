import java.util.Scanner;
public class ApplianceStore{
 public static void main(String[] args)
 {
  Scanner sc = new Scanner(System.in);
  
  Microwave[] micro = new Microwave[4];
  for(int i=0; i<micro.length; i++)
  {
   micro[i] = new Microwave();
   System.out.println("What is the brand of the microwave? ");
   micro[i].brand = sc.next();
   System.out.println("What is the material of the microwave? ");
   micro[i].material = sc.next();
   System.out.println("What is the price of the microwave? ");
   micro[i].price = sc.nextInt();
  }
  
   System.out.println(micro[micro.length-1].brand);
   System.out.println(micro[micro.length-1].material);
   System.out.println(micro[micro.length-1].price);
   
   micro[0].start();
   micro[0].quality(); 
 }
}